import React,{Component} from 'react';
import SpinnerLoad from '../components/spinner'
import Landing from './landing/Landing'
import Experiences from './experiences/Experiences'
import Projects from './projects/Projects'
import { IntroContainer, mainContainer, MenuContainer } from './style';
import { SocialIcon } from 'react-social-icons';
import AnchorLink from 'react-anchor-link-smooth-scroll'


class Home extends Component{
    constructor(props){
        super(props)
        this.state = {
            isLoading : true,
            isIntro:true
        }
        this.removeIntro.bind(this);
    }
    removeIntro(){
        setTimeout(
            function() {
                this.setState({isIntro: false});
            }
            .bind(this),
            4000
        );
        
    }
    componentDidMount(){
        setTimeout(
            function() {
                this.setState({isLoading: false});
            }
            .bind(this),
            2000
        );

        this.removeIntro();

    }
   
    render(){
    
        return(
            <IntroContainer>
                <div>
                {
                    this.state.isLoading ?
                    <SpinnerLoad /> : 
                    this.state.isIntro ?
                    <div className="wrapper">
                        <div className="portfolio-screen">
                        <img className="logoImg animation-opacity" src="/assets/img/logo.jpeg"></img>
                        <h1 className="text-portfolio"><span className="left animation-left">kartadibrata's</span><span className="right animation-right">portfolio</span></h1>
                        </div>
                     
                    </div>
                    :
                    <mainContainer>
                        <div className="wrapper-all">
                            <MenuBar />
                            <Landing href="#work-href" id="home"/>
                            <Experiences id="work-href" exp="experiences"/>
                            <Projects id="projects"/>
                           <Footer id="resume" />
                            
                        
                        </div>

                    </mainContainer>
                  
                    
                   
                }
                    
                </div>
            </IntroContainer>
        )
    }
}

class Footer extends React.Component{
    render(){
        return(
            <div className="footer-wrapper">
                 <div className="resume-wrapper" id={this.props.id}>
                                <h5>Click <a href="https://docs.google.com/document/d/1sbcdL1-qMUAcnRjOG8i-b37UG_QnlFddfOhTDAL5J0I/edit?usp=sharing" className="resume">here</a> to see my resume!</h5>
                            </div>
                            <div className="social-container">
                            <div className="social">
                                <SocialIcon url="http://linkedin.com/in/adykarta" network="linkedin" bgColor="grey" fgColor="white" style={{ height: 35, width: 35 }}/>
                            </div>
                             <div className="social">
                             <SocialIcon url="https://wa.me/6281317250076" network="whatsapp" bgColor="grey" fgColor="white"style={{ height: 35, width: 35 }}/>
                             </div>
                             <div className="social">
                             <SocialIcon url="https://gitlab.com/adykarta" network="github" bgColor="grey" fgColor="white" style={{ height: 35, width: 35 }}/>
                             </div>
                             <div className="social">
                             <SocialIcon url="https://www.instagram.com/adykartaa/" network="instagram" bgColor="grey" fgColor="white" style={{ height: 35, width: 35 }}/>
                             </div>
                             <div className="social">
                             <SocialIcon url="mailto:mkartadibrata@gmail.com" network="mailto" bgColor="grey" fgColor="white" style={{ height: 35, width: 35 }}/>
                             </div>
                               
                               
                               
                               

                            </div>
            </div>
            
        )
    }
}

class MenuBar extends React.Component{
    render(){
        return(
            <MenuContainer>
                <div className="menu-wrapper">
                <AnchorLink href="#home">
                <h6 class="menu-a">Home</h6>

                </AnchorLink>
                
                    <AnchorLink href="#experiences">
                    <h6 class="menu-b"> Experiences</h6>
                    </AnchorLink>
                    <AnchorLink href="#projects">
                    <h6 class="menu-c">Projects</h6>
                    </AnchorLink>
                    <AnchorLink href="#resume">
                    <h6 class="menu-d">Resume</h6>
                    </AnchorLink>
                
                </div>

            </MenuContainer>
        )
    }
}
export default Home;
import styled, {keyframes} from "styled-components";


export const ProjectsContainer = styled.div`
   .ProjectsWrapper{
       height:auto;
       background-color: #F5F5F5;
       display:flex;
       justify-content:center;
       text-align:center;
       padding-top:8%;
   }
   .text-bold{
    font-weight:bolder;
  }
  .project-container{
    width:100%;
    justify-content:center;
    align-items:center;
    display:flex;
    flex-direction: column;
    padding-top:4%;
    padding-bottom:4%;
  }
  .body-text{
    color:#797979;
    text-align:center;
  }
  .full-img{
    width:60%;
    height:auto;
  }
  #padding{
    padding-top:4px;
    padding-bottom:4px;
  }
  .projects-h1{
    font-size:2rem;
  }
  @media only screen and (max-width:767px){
    .projects-h1{
      font-size:1.8rem;
    }
  }
`


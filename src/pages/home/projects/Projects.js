import React,{Component} from 'react';
import {ProjectsContainer } from '../projects/style';

class Projects extends React.Component{
    render(){
        return(
            <ProjectsContainer>
                <div className="ProjectsWrapper">
                    <div className="container">
                        <h1 class="text-bold" id={this.props.id}>Projects</h1>
                        <ProjectContainer  title="Personal Website" image="/assets/img/personal.png" text="A Personal Website built using React JS"/>
                        <ProjectContainer  title="Risk Analytics - BPKP" image="/assets/img/bpkpproject.png" text="Risk Analytics Platform that belongs to BPKP (Badan Pengawasan Keuangan dan Pembangunan). Built using Laravel and MariaDB for database"/>
                        <ProjectContainer  title="Business Law Society FH UI" image="/assets/img/blsproject.png" text="An Organization Profile of Business Law Society Faculty of Law UI. Built using Django"/>
                        <ProjectContainer  title="MWA UIUM" image="/assets/img/mwaproject.png" text="An Organization Profile of MWA UI UM (Unsur Mahasiswa). Built using Django"/>
                        <ProjectContainer  title="Admin Page - bagidata" image="/assets/img/bagidataproject.png" text="Bagidata admin page that contains KTP verification, Scrapper Feature, and Dashboard. Built using React JS and Redux"/>


                    </div>

                    
                </div>
            </ProjectsContainer>

        )
    }
}

class ProjectContainer extends React.Component{
    render(){
        return(
           <div className="project-container">
                    <h1 id="padding" className="text-bold projects-h1">{this.props.title}</h1>
                    <div className="image-container">
                        <img id="padding" src={this.props.image} className="full-img"></img>

                    </div>

                    <h5  id="padding" className="body-text">{this.props.text}</h5>
           </div>
        )
    }
}

export default Projects;
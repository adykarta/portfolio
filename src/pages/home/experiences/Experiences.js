import React,{Component} from 'react';
import {ExperiencesContainer, CardExperiencesContainer, WorkExperiencesContainer, WorkCardContainer} from './style';
import Logo from './../../components/logo'


class Experiences extends React.Component{
    render(){
        return(
           <ExperiencesContainer>
               <div className="experiencesContainer">
                    <div className="container-fluid">
                        <div className="field">
                            <h1 className="header-field" id={this.props.exp}>Education</h1>
                            <CardExperiences location="Depok, Indonesia" time="August 2017 - Present"  image="/assets/img/ui.png" subbody="Passed Courses: " title="Computer Science" place="University of Indonesia" bodytext="Basic of Programming Language 1 (Python), Basic of Programming Language 2 (Java), Data Structures and ALgorithm,
                                     Introduction to Computer Organization, Linear Algebra, Statistics & Probability, Web Programming & Design, Advanced Programming, Database, Operating Systems, Theory of Language & Automata "/>

                        </div> 
                        <div className="field">
                            <h1 className="header-field">Organization</h1>
                            <CardExperiences location="Depok, Indonesia" time="March 2019 - Present"  image="/assets/img/ristek.svg" title="Member of Project Management" place="RISTEK Fasilkom UI" subbody="What did i do: " bodytext="Become the Project Manager for internal or external projects in RISTEK and Managing Ristek TechTalk (seminar)"/>

                        </div> 
                       

                    </div>
                    <WorkExperiences id={this.props.id} />
                  
               </div>
             

           </ExperiencesContainer>
        )
    }
}
class WorkExperiences extends React.Component{
    render(){
        return(
            <WorkExperiencesContainer>
                 
                <div className="container-fluid">
                <div className="field">
                    <h1 className="header-field" id={this.props.id}>Works</h1>
                </div>
                  
                </div>

               
                <div className="box">
                    <div className="card-a">
                        <div className="row full-height">
                            <div className="col-md-2 col-arrow">
                                <div className="center arrow-container">
                                    <h1 className="arrow">&#8674;</h1>
                                </div>
                                
                            </div>
                            <div className="col-md-8">
                                <div className="work-project">
                                    <div className="col-md-4">
                                        <div className="work-container">
                                            <WorkCard title="Front End Engineer Intern" place="bagidata" time="June 2019 - September 2019" logoimg="/assets/img/bagidata.png" image="/assets/img/bagidataandroid.png" text="​Implement new design for Bagitrip Feature for Android using React Native and ​Implement new design for Bagidata Admin web app using ReactJS"/>
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                    <div className="work-container">
                                    <WorkCard title="Front End Developer Part Time" place="adaTeman" time="February 2019 - June 2019" logoimg="/assets/img/adateman.png" image="/assets/img/adatemanproject2.png" text="Creating and Maintaining adaTeman official website​ ​using ReactJS"/>
                                        
                                    </div>
                                    </div>
                                    <div className="col-md-4">
                                    <div className="work-container">
                                    <WorkCard title="Front End Developer Intern" place="adaTeman" time="January 2019 - February 2019" logoimg="/assets/img/adateman.png" image="/assets/img/adatemanproject.png" text="​Creating adaTeman official websit using ReactJS."/>
                                    
                                    </div>
                                    </div>
                                    
                                    
                                  
                                </div>

                               
                            </div>
                            <div className="col-md-2 col-arrow">
                                <div className="center arrow-container">
                                    <h1 className="arrow-back">&#8672;</h1>
                                </div>
                            </div>
                        </div>
                            
                       
                       
                          
        
                    
                    </div>
                   
                </div>

            </WorkExperiencesContainer>
         
        )
    }

}

class WorkCard extends React.Component{
    render(){
        return(
            <WorkCardContainer>
                <div className="container-card">
                    <Logo image={this.props.logoimg} id="padding"/>
                    <h1 id="padding" className="text-bold work-h1">{this.props.title}</h1>
                    <h3 id="padding" className="text-bold work-h3">{this.props.place}</h3>
                    <h6  className="work-h6" id="padding">{this.props.time}</h6> 
                    <img id="padding" src={this.props.image} className="full-img"></img>
                    <h5  id="padding" className="body-text work-h5">{this.props.text}</h5>
                </div>

            </WorkCardContainer>
        )
    }
}

class CardExperiences extends React.Component{
    render(){
        return(
            <CardExperiencesContainer>
                <div className="cardExperiencesContainer">
                    <div className="row">
                        <div className="col-md-3">
                            <h6>{this.props.location}</h6>
                            <h6>{this.props.time}</h6>
                        </div>
                        <div className="col-md-1">
                            <Logo image={this.props.image}/>
                        </div>
                        <div className="col-md-8">
                            <div className="row">
                                <h5>{this.props.title} &#160; &#8212; &#160;<span className="text-bold">{this.props.place}</span></h5>
                              
                             
                               
                            </div>
                            <div className="row">
                                <h6 className="text-bold body-text">{this.props.subbody}</h6>
                            </div>
                            <div className="row">
                                <h6 className="body-text">{this.props.bodytext}</h6>
                            </div>
                          
                        </div>
                    </div>
                </div>

            </CardExperiencesContainer>
        
        )   
    }
}
export default Experiences;
import styled, {keyframes} from "styled-components";



export const ExperiencesContainer = styled.div`
  .experiencesContainer{
       height:auto;
       background-color: #F5F5F5;
       overflow-x:hidden;
   
  }
  .container-fluid{
    padding-left:10%;
    padding-right:10%;
  }
  .field{
    padding-top:4%;
    margin-bottom:0;
  }
  .text-bold{
    font-weight:bolder;
  }
  .header-field{
    font-weight:bolder;
    padding-bottom:4%;
  }

  @media only screen and (max-width:767px){
    .field{
      margin-bottom:8%;
    }
  }

    
`
export const WorkCardContainer = styled.div`
.container-card{
  height:100%;
  width:100%;
  justify-content:center;
  align-items:center;
  display:flex;
  flex-direction: column;
}
.body-text{
  color:#797979;
  text-align:center;
}
.full-img{
  width:60%;
  height:auto;
}
#padding{
  padding-top:4px;
  padding-bottom:4px;
}
.work-h1{
  font-size:2rem;
}
.work-h3{
  font-size:1.4rem;
}
.work-h6{
  font-size:0.8rem;
}
.work-h5{
  font-size:1rem;
}
@media only screen and (max-width:767px){
  .work-h1{
    font-size:1.8rem;
  }
  .work-h3{
    font-size:1.25rem;
  }
  .work-h6{
    font-size:0.8rem;
  }
  .work-h5{
    font-size:1rem;
  }


}
`

export const bouncearrow = keyframes`
    0%{
        left:0px;
       
    }
    50%{
        left:50px;
    }
    100%{
        left:0px;
    }
`
export const bouncearrowback = keyframes`
    0%{
        right:0px;
       
    }
    50%{
        right:50px;
    }
    100%{
        right:0px;
    }
`
export const WorkExperiencesContainer = styled.div`
 .box{
   height:100vh;
   background-color:#F5F5F5;
   overflow-x:scroll;
   overflow-y:hidden;
   flex-wrap:nowrap;

 }
 .work-container{
   display:inline-block;

 }
 .work-project{
   height:100%;
   justify-content:space-between;
   display:flex;
   align-items:center;


 }
 .box::-webkit-scrollbar{
   display:none;
 }
.col-arrow{
  width:100%;
}
 .card-a{
  background-color:#F5F5F5;
  height:100%;
  width:200vw;
  justify-content:center;
  align-items:center;
 }
 .full-height{
   height:100%;
   flex-wrap:nowrap !important
 }
 .arrow{
   font-size:700%;
  display: inline-block;
  width: 100px;
  position: relative;
  animation-name: ${bouncearrow};
  animation-duration: 2s;
  animation-timing-function: ease;
  animation-delay: 0s;
  animation-iteration-count: infinite;
  animation-direction: normal;
  animation-fill-mode: forwards;
  animation-play-state: running;

}
.arrow-back{
  font-size:700%;
  display: inline-block;
  width: 100px;
  position: relative;
  animation-name: ${bouncearrowback};
  animation-duration: 2s;
  animation-timing-function: ease;
  animation-delay: 0s;
  animation-iteration-count: infinite;
  animation-direction: normal;
  animation-fill-mode: forwards;
  animation-play-state: running;
}
.center{
  height:100%;
  display:flex;
  justify-content:center;
  align-items:Center;
}
@media only screen and (max-width:767px){
  .box{
    height:100vh;
    
  }
  .card-a{
    width:400vw;
  }
  .col-arrow{
    width:10%;
  }

}

`


export const CardExperiencesContainer = styled.div`
  .cardExperiencesContainer{
       height:auto;
       background-color: #F5F5F5;
       padding-left:4%;
  }
  .body-text{
    color:#797979;
  }
    
`
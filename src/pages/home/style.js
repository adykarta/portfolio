import React from "react";
import styled, {keyframes} from "styled-components";

export const opac = keyframes`
    0%{
        opacity:0;
    }
    100%{
        opacity:1;
    }
`
export const animateleft = keyframes`
0% {
    left: -300px;
    opacity: 0;
}

100% {
    left: 0;
    opacity: 1;
}


`

export const animateright = keyframes`
0% {
    right: -300px;
    opacity: 0;
}
100% {
    right: 0;
    opacity: 1;
}

`

export const mainContainer = styled.div`
    height:auto;
    width:100vw;

`

export const IntroContainer = styled.div`
    .wrapper-all{
        background-color:#F5F5F5;
        overflow-x:hidden;
    }

    .wrapper{
        height:100vh;
        width:100vw;
        display: flex;
        justify-content: center;
        align-items: center;
        text-align: center;
        background-color:white;
        
    }
    .portfolio-screen{
        display-flex;
        flex-direction:row;
    }
    .animation-opacity{
        position:relative;
        animation-duration: 0.8s;
        animation-timing-function: ease;
        animation-delay: 0s;
        animation-iteration-count: 1;
        animation-direction: normal;
        animation-fill-mode: none;
        animation-play-state: running;
        animation-name: ${opac};
    }
    .animation-left{
        position:relative;
        animation-duration: 0.4s;
        animation-timing-function: ease;
        animation-delay: 0s;
        animation-iteration-count: 1;
        animation-direction: normal;
        animation-fill-mode: none;
        animation-play-state: running;
        animation-name: ${animateleft};
    }
    .animation-right{
        position:relative;
        animation-duration: 0.4s;
        animation-timing-function: ease;
        animation-delay: 0s;
        animation-iteration-count: 1;
        animation-direction: normal;
        animation-fill-mode: none;
        animation-play-state: running;
        animation-name: ${animateright};
    }
    .left{
        font-weight: bold;
    }     

    .logoImg{
        max-width:300px;
        max-height:300px;
        padding-bottom:2%;
    
    }
    .resume{
        cursor:pointer;
        color:black;
        font-weight:bolder;
        
    }
    .resume-wrapper{
        padding:2%;
        background-color: #F5F5F5;

    }
    .social-container{
        background-color:#F5F5F5;
        padding:2%;
        display:flex;
        
    }
    .social{
        padding-right:4px;
        justify-content:inline-block;
    }
    .text-portfolio{
        font-size:2.5rem;
    }
    .footer-wrapper{
        height:auto;
    }
    @media only screen and (max-width:414px){
        .logoImg{
            max-width:200px;
            max-height:200px;

        }
        .text-portfolio{
            font-size:1.8rem;
        }
    }







    `

export const MenuContainer = styled.div`
.menu-wrapper{
background-color: transparent;
position:fixed;
top: 20%;
left:3%;
z-index:100000;

display:flex;
flex-direction:column;

}
.menu-a,.menu-b,.menu-c,.menu-d{
    font-weight:bolder;
    cursor:pointer;
    transition: 0.3s;
    color:black;
    z-index:100000;
}
.menu-a:hover,.menu-b:hover,.menu-c:hover,.menu-d:hover{
    font-size:2rem;
}

`

import styled, {keyframes} from "styled-components";

export const bouncearrow = keyframes`
    0%{
        top:0px;
       
    }
    50%{
        top:50px;
    }
    100%{
        top:0px;
    }
`

export const animateright = keyframes`
0% {
    right: -300px;
    opacity: 0;
}
100% {
    right: 0;
    opacity: 1;
}
`
export const HomeContainer = styled.div`

    .HomeWrapper{
        background-color: #F5F5F5
        height:100vh;
        width:100%;
       
    }

    .main-box{
        justify-content:center;
        display:flex;
        height:100vh;
        align-items:center;
        padding:auto;
    }

    .img-ady{
        width:100%;
        height:auto;
    }
    .landing-text{
        display:flex;
        justify-content:center;
        align-items:center;
        flex-direction:column;
        padding:auto;
       
    }
    .landing-name{
        font-size:3rem;
    }
    .landing-about{
        font-size:1.25rem;
    }
    .text-bold{
        font-weight:bolder;
    }
    .arrow{
        cursor:pointer;
        display: inline-block;
        width: 100px;
        position: relative;
        animation-name: ${bouncearrow};
        animation-duration: 2s;
        animation-timing-function: ease;
        animation-delay: 0s;
        animation-iteration-count: infinite;
        animation-direction: normal;
        animation-fill-mode: forwards;
        animation-play-state: running;
        color:black;
     
    }
    .animation-right{
        position:relative;
        animation-duration: 0.4s;
        animation-timing-function: ease;
        animation-delay: 0s;
        animation-iteration-count: 1;
        animation-direction: normal;
        animation-fill-mode: none;
        animation-play-state: running;
        animation-name: ${animateright};
    }

    @media only screen and (max-width:767px){
        .landing-text{
            padding:8%;
        }
        .main-box{
            padding:22%;
        }
        .landing-name{
            font-size:1.4rem;
        }
        .landing-about{
            font-size:0.8rem;
        }
       
    }
    
`
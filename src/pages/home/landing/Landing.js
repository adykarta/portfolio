import React,{Component} from 'react';
import {HomeContainer } from '../landing/style';
import AnchorLink from 'react-anchor-link-smooth-scroll';

class Landing extends React.Component{
    render(){
        return(
            <HomeContainer>
                <div className="HomeWrapper" id={this.props.id}>
                    <div className="main-box">
                        <div className="container">
                            <div className="row">
                                <div className="col-md-6">
                                    <img className="img-ady animation-opacity" src="/assets/img/ady.png"></img>
                                </div>
                                <div className="col-md-6 landing-text">
                                    <div className="row">
                                        <h2 className="animation-right landing-name">Hello, My name is <span className="text-bold">Muhamad Istiady Kartadibrata</span></h2>
                                    </div>
                                    <div className="row">
                                        <h6 className="animation-right landing-about">I am a <span  className="text-bold">Computer Science</span> undergraduate student at <span  className="text-bold">Univesitas Indonesia</span> in 3rd years.</h6>

                                    </div>     
                                    <div className="row">
                                        <div >
                                            <AnchorLink href={this.props.href}>
                                                <h1 className="arrow">&#8675;</h1>
                                            </AnchorLink>
                                         
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>

                        
                        </div>
                    </div>
                    
                </div>
            </HomeContainer>

        )
    }
}

export default Landing;
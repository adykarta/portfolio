import React, {Component} from 'react'
import styled from "styled-components";

const SpinnerContainer = styled.div`
    .logo {
        animation: logo-spin infinite 7s linear;
        height: 40vmin;
        pointer-events: none;
        max-width:300px;
        max-height:300px;
    }
    .wrapper{
        height:100vh;
        width:100vw;
        display: flex;
        justify-content: center;
        align-items: center;
        text-align: center;
        background-color:white;
    }

    @keyframes logo-spin {
        from {
          transform: rotate(0deg);
        }
        to {
          transform: rotate(360deg);
        }
      }
`

class SpinnerLoad extends Component{
    render(){
        return(
            <SpinnerContainer>
                <div className="wrapper">
                    <img src="https://image.flaticon.com/icons/svg/206/206078.svg" className="logo"></img>
                </div>
            </SpinnerContainer>
        )
    }
}
export default SpinnerLoad
import React, {Component} from 'react'
import styled from "styled-components";

const LogoContainer = styled.div`
    .logo-container {
        max-width:3rem;
        max-height:3rem;
        margin-top:0;
        margin-bottom:0;
        
        display:flex;
        justify-content:center;
        align-items:center;
    }
    .logo{
        width:100%;
        height:100%;
    }
    @media only screen and (max-width:767px){
        .logo-container{
            margin-bottom:4%;
            margin-top:4%
        }
    }
    
 
`

class Logo extends Component{
    render(){
        return(
            <LogoContainer>
                <div className="logo-container">
                    <img className="logo" src={this.props.image}></img>
                </div>
               
            </LogoContainer>
        )
    }
}
export default Logo